<?php 

namespace DarioRieke\Kernel\Exception\Http;

use DarioRieke\Kernel\Exception\Http\HttpException;

/**
 * 404 - not found
 */
class NotFoundException extends HttpException {
	
	/**
	 * create a new NotFoundException
	 * @param string|null     $message  internal message
	 * @param \Throwable|null $previous previous Exception
	 * @param int|integer     $code     internal error code
	 */
	function __construct(string $message = null, \Throwable $previous = null, int $code = 0) {
		parent::__construct(404, $message, $previous, $code);
	}
}
