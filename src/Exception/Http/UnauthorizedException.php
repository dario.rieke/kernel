<?php 

namespace DarioRieke\Kernel\Exception\Http;

use DarioRieke\Kernel\Exception\Http\HttpException;

/**
 * 401 - unauthorized
 */
class UnauthorizedException extends HttpException {
	
	/**
	 * create a new UnauthorizedException
	 * @param string|null     $message  internal message
	 * @param \Throwable|null $previous previous Exception
	 * @param int|integer     $code     internal error code
	 */
	function __construct(string $message = null, \Throwable $previous = null, int $code = 0) {
		parent::__construct(401, $message, $previous, $code);
	}
}
