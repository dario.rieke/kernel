<?php 

namespace DarioRieke\Kernel\Exception\Http;

use DarioRieke\Kernel\Exception\Http\HttpException;

/**
 * 405 - method not allowed
 */
class MethodNotAllowedException extends HttpException {
	
	/**
	 * create a new MethodNotAllowedException
	 * @param string|null     $message  internal message
	 * @param \Throwable|null $previous previous Exception
	 * @param int|integer     $code     internal error code
	 */
	function __construct(string $message = null, \Throwable $previous = null, int $code = 0) {
		parent::__construct(405, $message, $previous, $code);
	}
}
