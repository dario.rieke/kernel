<?php 

namespace DarioRieke\Kernel\Exception\Http;

use DarioRieke\Kernel\Exception\Http\HttpExceptionInterface;


/**
 * HttpException
 */
class HttpException extends \RuntimeException implements HttpExceptionInterface {

	/**
	 * HTTP status code
	 * @var int
	 */
	private $statusCode;

	/**
	 * create a new HttpException
	 * @param int             $statusCode HTTP status code
	 * @param string|null     $message    internal message
	 * @param \Throwable|null $previous   optional previous Exception
	 * @param int             $code       internal error code
	 */
	public function __construct(int $statusCode, string $message = null, \Throwable $previous = null, ?int $code) {
		$this->statusCode = $statusCode;
		parent::__construct($message, $code, $previous);
	}

	public function getStatusCode(): int {
		return $this->statusCode;
	}
}
 ?>