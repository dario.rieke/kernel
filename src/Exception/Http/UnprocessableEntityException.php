<?php 

namespace DarioRieke\Kernel\Exception\Http;

use DarioRieke\Kernel\Exception\Http\HttpException;

/**
 * 422 - unprocessable entity 
 */
class UnprocessableEntityException extends HttpException {
	
	/**
	 * create a new UnprocessableEntityException
	 * @param string|null     $message  internal message
	 * @param \Throwable|null $previous previous Exception
	 * @param int|integer     $code     internal error code
	 */
	function __construct(string $message = null, \Throwable $previous = null, int $code = 0) {
		parent::__construct(422, $message, $previous, $code);
	}
}
