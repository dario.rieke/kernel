<?php 
namespace DarioRieke\Kernel;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use DarioRieke\EventDispatcher\EventDispatcherInterface;
use DarioRieke\Router\RouterInterface;
use DarioRieke\CallableResolver\CallableResolverInterface;
use DarioRieke\CallableResolver\ArgumentResolverInterface;
use DarioRieke\Kernel\Event\KernelEvents;
use DarioRieke\Kernel\KernelInterface;
use DarioRieke\Kernel\Event\RequestEvent;
use DarioRieke\Kernel\Event\ControllerEvent;
use DarioRieke\Kernel\Event\ControllerArgumentsEvent;
use DarioRieke\Kernel\Event\ResponseEvent;
use DarioRieke\Kernel\Event\ExceptionEvent;
use DarioRieke\Kernel\Event\ViewEvent;
use DarioRieke\Kernel\Event\FinishRequestEvent;
use DarioRieke\Kernel\Exception\NoResponseException;

/**
 * Kernel
 */
class Kernel implements KernelInterface {

	/**
	 * @var EventDispatcherInterface
	 */
	protected $dispatcher;

	/**
	 * @var RouterInterface
	 */
	protected $router;

	/**
	 * @var CallableResolverInterface
	 */
	protected $callableResolver;

	/**
	 * @var ArgumentResolverInterface
	 */
	protected $argumentResolver;

	/**
	 * construct the framework and inject dependencies 
	 * @param EventDispatcherInterface	  $dispatcher 
	 * @param RouterInterface       	  $router 
	 * @param CallableResolverInterface $callableResolver 
	 * @param ArgumentResolverInterface $callableResolver 
	 */
	public function __construct(
		EventDispatcherInterface $dispatcher,
		RouterInterface $router,
		CallableResolverInterface $callableResolver, 
		ArgumentResolverInterface $argumentResolver
	) {
		$this->dispatcher = $dispatcher;
		$this->router = $router;
		$this->callableResolver = $callableResolver;
		$this->argumentResolver = $argumentResolver;
	}

	/**
	 * @param  ServerRequestInterface $request 
	 * @return ResponseInterface
	 */
	public function handle(ServerRequestInterface $request): ResponseInterface {
		try {
			return $this->handleInternal($request);
		}
		catch (\Exception $e) {
			return $this->handleThrowable($request, $e);	
		}
	}

	/**
	 * handle a request and return a response internal
	 * @param  ServerRequestInterface $request
	 * @return ResponseInterface
	 * @throws KernelExceptionInterface
	 */
	private function handleInternal(ServerRequestInterface $request): ResponseInterface {

		//dispatch Request Event
		$requestEvent = new RequestEvent($this, $request);
		$this->dispatcher->dispatch($requestEvent, KernelEvents::REQUEST);
		if($requestEvent->hasResponse()) {
			return $requestEvent->getResponse();
		}

		//get the controller
		$result = $this->router->match($request->getRequestTarget(), $request->getMethod());
		$controller = $result->getCallback();
		$controller = $this->callableResolver->resolveCallable($controller);
		$controllerEvent = new ControllerEvent($this, $request, $controller);
		$this->dispatcher->dispatch($controllerEvent, KernelEvents::CONTROLLER);
		$controller = $controllerEvent->getController();

		//resolve arguments
		$arguments = $this->argumentResolver->resolveArguments($request, $controller);
		$controllerArgumentsEvent = new ControllerArgumentsEvent($this, $request, $arguments);
		$this->dispatcher->dispatch($controllerArgumentsEvent, KernelEvents::CONTROLLER_ARGUMENTS);
		$arguments = $controllerArgumentsEvent->getArguments();

		//get the response
		$response = $controller(...$arguments);
		if(!$response instanceof ResponseInterface) {
			// if no response was returned, convert the return value to a response
			$event = new ViewEvent($this, $request, $response);
			$this->dispatcher->dispatch($event, KernelEvents::VIEW);

			if($event->hasResponse()) {
				$response = $event->getResponse();
			}
			else {
				throw new NoResponseException("None of the components generated a response.");
			}
		}
		else {
			//fire event when controller returned a real PSR-7 response
			$responseEvent = new ResponseEvent($this, $request, $response);
			$this->dispatcher->dispatch($responseEvent, KernelEvents::RESPONSE);
			$response = $responseEvent->getResponse();
		}

		// $this->finish($request, $response);
		
		return $response;
	}

	/**
	 * turn an Exception into a Response
	 * @param  \Exception $e 
	 * @param  ServerRequestInterface
	 * @return ResponseInterface        
	 */
	private function handleThrowable(ServerRequestInterface $request, \Exception $e): ResponseInterface {

		$event = new ExceptionEvent($this, $request, $e);
		$this->dispatcher->dispatch($event, KernelEvents::EXCEPTION);

		if($event->hasResponse()) {
			return $event->getResponse();
		}
		else {
			// $this->finish($request, $response);
			throw $e;
		}
	}

	/**
	 * finish the current request by dispatching a finish event
	 * @return 
	 */
	private function finish(ServerRequestInterface $request, ResponseInterface $response) {
		$event = new FinishRequestEvent($this, $request, $response);
		$this->dispatcher->dispatch($event, KernelEvents::FINISH);
	}
}
?>