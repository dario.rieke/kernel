<?php 
namespace DarioRieke\Kernel\Event;

use DarioRieke\Kernel\Event\KernelEvent;
use DarioRieke\Kernel\KernelInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ControllerArgumentsEvent
 */
class ControllerArgumentsEvent extends KernelEvent {

	/**
	 * @var array
	 */
	private $arguments;

	/**
	 * @param KernelInterface 			$kernel    
	 * @param ServerRequestInterface    $request   
	 * @param array      				$arguments
	 */
	public function __construct(KernelInterface $kernel, ServerRequestInterface $request, array $arguments) {
		parent::__construct($kernel, $request);
		$this->setArguments($arguments);
	}

	/**
	 * set the arguments
	 * @param array $arguments
	 */
	public function setArguments(array $arguments) {
		$this->arguments = $arguments;
	}

	/**
	 * get the arguments
	 * @return array
	 */
	public function getArguments(): array {
		return $this->arguments;
	} 
}
?>