<?php 
namespace DarioRieke\Kernel\Event;

use DarioRieke\Kernel\Event\KernelEvent;
use Psr\Http\Message\ResponseInterface;
use DarioRieke\Kernel\KernelInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ResponseEvent
 */
class ResponseEvent extends KernelEvent {
	
	public function __construct(KernelInterface $kernel, ServerRequestInterface $request, ResponseInterface $response ) {
		$this->setResponse($response);
		parent::__construct($kernel, $request);
	}

	/**
	 * @var ResponseInterface
	 */
	private $response;

	/**
	 * returns the current Response
	 * @return ResponseInterface
	 */
	public function getResponse(): ResponseInterface {
		return $this->response;
	}

	/**
	 * set a Response for this Event
	 * @param ResponseInterface $response
	 */
	public function setResponse(ResponseInterface $response) {
		$this->response = $response;
	}

}
?>