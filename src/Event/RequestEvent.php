<?php 
namespace DarioRieke\Kernel\Event;

use DarioRieke\Kernel\Event\KernelEvent;
use Psr\Http\Message\ResponseInterface;

/**
 * RequestEvent
 */
class RequestEvent extends KernelEvent {

	/**
	 * @var ResponseInterface
	 */
	private $response;

	/**
	 * returns the current Response
	 * @return ResponseInterface
	 */
	public function getResponse(): ResponseInterface {
		return $this->response;
	}

	/**
	 * set a Response for this Event
	 * @param ResponseInterface $response
	 */
	public function setResponse(ResponseInterface $response) {
		$this->response = $response;
	}

	/**
	 * check if a Response exists for this Event
	 * @return boolean
	 */
	public function hasResponse(): bool {
		return null !== $this->response;
	}
}
?>