<?php 
namespace DarioRieke\Kernel\Event;

use DarioRieke\Kernel\Event\RequestEvent;
use Psr\Http\Message\ResponseInterface;
use DarioRieke\Kernel\KernelInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ViewEvent
 */
class ViewEvent extends RequestEvent {

	/**
	 * controller value
	 * @var mixed
	 */
	private $controllerValue;

	/**
	 * create new View Event
	 * @param KernelInterface 		 $kernel  
	 * @param ServerRequestInterface $request
	 * @param mixed 		  		 $controllerValue	the return value of the controller 
	 * 													which should be turned into a response 
	 */
	public function __construct(KernelInterface $kernel, ServerRequestInterface $request, $controllerValue) {
		$this->setControllerValue($controllerValue);
		parent::__construct($kernel, $request);
	}

	public function getControllerValue() {
		return $this->controllerValue;
	}

	public function setControllerValue($value) {
		$this->controllerValue = $value;
	}
}
?>