<?php 
namespace DarioRieke\Kernel\Event;

use DarioRieke\Kernel\Event\KernelEvent;
use DarioRieke\Kernel\KernelInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ControllerEvent
 */
class ControllerEvent extends KernelEvent {

	/**
	 * @var callable
	 */
	private $controller;

	/**
	 * @param KernelInterface		 $kernel    
	 * @param ServerRequestInterface $request   
	 * @param callable       		 $controller
	 */
	public function __construct(KernelInterface $kernel, ServerRequestInterface $request, callable $controller) {
		parent::__construct($kernel, $request);
		$this->setController($controller);
	}

	/**
	 * set the controller
	 * @param callable $controller
	 */
	public function setController(callable $controller) {
		$this->controller = $controller;
	}

	/**
	 * get the controller
	 * @return callable
	 */
	public function getController(): callable {
		return $this->controller;
	} 
}
?>