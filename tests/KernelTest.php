<?php 
namespace DarioRieke\Kernel\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\Kernel\Kernel;
use DarioRieke\Kernel\KernelInterface;
use Psr\Http\Server\RequestHandlerInterface;
use DarioRieke\EventDispatcher\EventDispatcherInterface;
use DarioRieke\EventDispatcher\EventDispatcher;
use DarioRieke\Router\RouterInterface;
use DarioRieke\CallableResolver\CallableResolverInterface;
use DarioRieke\CallableResolver\ArgumentResolverInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use DarioRieke\Kernel\Event\RequestEvent;
use DarioRieke\Kernel\Exception\NoResponseException;
use DarioRieke\Kernel\Event\KernelEvents;


class KernelTest extends TestCase {

    public function getKernel($dispatcher = null, $controller = null, $arguments = []) {
        $controller = $controller ?? function() {};

        $callableResolver = $this->getMockBuilder(CallableResolverInterface::class)->getMock();
        $callableResolver
            ->expects($this->any())
            ->method('resolveCallable')
            ->willReturn($controller);

        $argumentResolver = $this->getMockBuilder(ArgumentResolverInterface::class)->getMock();
        $argumentResolver
            ->expects($this->any())
            ->method('resolveArguments')
            ->willReturn($arguments);
        return new Kernel(
            $dispatcher ?? $this->getEventDispatcherMock(),
            $router ?? $this->getRouterMock(),
            $callableResolver,
            $argumentResolver 
        );
    }

    public function getEventDispatcherMock() {
        return $this->createMock(EventDispatcherInterface::class);
    }

    public function getEventDispatcher() {
        return new EventDispatcher();
    }

    public function getRouterMock() {
        return $this->createMock(RouterInterface::class);
    }

    public function getRequestMock($target = '/', $method = 'GET') {
        $mock = $this->createMock(ServerRequestInterface::class);
        $mock->method('getRequestTarget')->willReturn($target);
        $mock->method('getMethod')->willReturn($method);
        return $mock;
    }

    public function getResponseMock($body, $status = 200) {
        $mock = $this->createMock(ResponseInterface::class);
        $mock->method('getBody')->willReturn($body, $status);
        return $mock;
    }

	public function testImplementsKernelInterface() {
		$this->assertInstanceOf(KernelInterface::class, $this->getKernel());
    }
    public function testImplementsRequestHandlerInterface() {
		$this->assertInstanceOf(RequestHandlerInterface::class, $this->getKernel());
    }

    public function testThrowsExceptionIfNoResponseWasGenerated() {
        $kernel = $this->getKernel();
        $this->expectException(NoResponseException::class);
        $kernel->handle($this->getRequestMock());
    }

    public function testReturnsResponseFromResponseEventListenerIfSet() {
        $dispatcher = $this->getEventDispatcher();
        $dispatcher->addListener(KernelEvents::REQUEST, function($event) {
            $event->setResponse($this->getResponseMock('foobarbaz'));
        });

        $kernel = $this->getKernel($dispatcher);
        $response = $kernel->handle($this->getRequestMock());
        $this->assertEquals('foobarbaz', $response->getBody());
    }

    public function testLetsControllerEventAlterTheController() {
        $controller = function() { 
            return $this->getResponseMock('foo'); 
        };
        $dispatcher = $this->getEventDispatcher();
        $dispatcher->addListener(KernelEvents::CONTROLLER, function($event) {
            $event->setController(function() { 
                return $this->getResponseMock('baz'); 
            });
        });

        $kernel = $this->getKernel($dispatcher, $controller);
        $response = $kernel->handle($this->getRequestMock());
        $this->assertEquals('baz', $response->getBody());
    }

    public function testLetsControllerArgumentsEventAlterControllerArguments() {
        $controller = function($content) { 
            return $this->getResponseMock($content); 
        };
        $dispatcher = $this->getEventDispatcher();
        $dispatcher->addListener(KernelEvents::CONTROLLER_ARGUMENTS, function($event) {
            $event->setArguments(['foo']);
        });

        $kernel = $this->getKernel($dispatcher, $controller, ['bar']);
        $response = $kernel->handle($this->getRequestMock());
        $this->assertEquals('foo', $response->getBody());
    }

    public function testLetsViewEventTurnControllerReturnValueIntoActualResponse() {
        $controller = function() { 
            return 'bar';
        };
        $dispatcher = $this->getEventDispatcher();
        $dispatcher->addListener(KernelEvents::VIEW, function($event) {
            $controllerValue = $event->getControllerValue();
            $event->setResponse($this->getResponseMock($controllerValue));
        });

        $kernel = $this->getKernel($dispatcher, $controller);
        $response = $kernel->handle($this->getRequestMock());
        $this->assertEquals('bar', $response->getBody());
    }

    public function testLetsResponseEventChangeControllerReturnedResponse() {
        $controller = function() { 
            return $this->getResponseMock('bar');
        };
        $dispatcher = $this->getEventDispatcher();
        $dispatcher->addListener(KernelEvents::RESPONSE, function($event) {
            $event->setResponse($this->getResponseMock('baz'));
        });

        $kernel = $this->getKernel($dispatcher, $controller);
        $response = $kernel->handle($this->getRequestMock());
        $this->assertEquals('baz', $response->getBody());
    }

    public function testLetsExceptionEventHandleResponseOnException() {
        $controller = function() { 
            throw new \Exception('');
        };
        $dispatcher = $this->getEventDispatcher();
        $dispatcher->addListener(KernelEvents::EXCEPTION, function($event) {
            $event->setResponse($this->getResponseMock('custom error message'));
        });

        $kernel = $this->getKernel($dispatcher, $controller);
        $response = $kernel->handle($this->getRequestMock());
        $this->assertEquals('custom error message', $response->getBody());
    }

    public function testRethrowsErrorIfExceptionEventCouldNotGenerateResponse() {
        $controller = function() { 
            throw new \Exception('');
        };
        $kernel = $this->getKernel(null, $controller);
        $this->expectException(\Exception::class);
        $response = $kernel->handle($this->getRequestMock());
    }
}